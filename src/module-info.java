module project {
	exports termProject;
	
	requires javafx.controls;
	requires javafx.base;
	requires javafx.graphics;
	requires java.desktop;
	requires javafx.media;
}